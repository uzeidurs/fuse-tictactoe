var Observable = require('FuseJS/Observable');


// game board setup

var cellMap = {
	'TL':0,
	'TC':1,
	'TR':2,
	'CL':3,
	'CC':4,
	'CR':5,
	'BL':6,
	'BC':7,
	'BR':8,
};

var cells = Observable();
for (var label in cellMap) {
	cells.add(new Cell(label));
}


// game board helpers

function Cell(id) {
	this.id = id;
	this.content = Observable('');
};

function cellIndex(label) {
	return cellMap[label];
};

function isInLine() {
	var filledCells = {};
	cells.forEach(function(cell) {
		if (cell.content.value != '') filledCells[cell.id] = cell.content.value;
	});
	if (filledCells['TL'] != undefined && (filledCells['TL'] == filledCells['TC'] && filledCells['TC'] == filledCells['TR'])) return true;
	if (filledCells['TL'] != undefined && (filledCells['TL'] == filledCells['CL'] && filledCells['CL'] == filledCells['BL'])) return true;
	if (filledCells['TL'] != undefined && (filledCells['TL'] == filledCells['CC'] && filledCells['CC'] == filledCells['BR'])) return true;
	if (filledCells['TC'] != undefined && (filledCells['TC'] == filledCells['CC'] && filledCells['CC'] == filledCells['BC'])) return true;
	if (filledCells['TR'] != undefined && (filledCells['TR'] == filledCells['CC'] && filledCells['CC'] == filledCells['BL'])) return true;
	if (filledCells['TR'] != undefined && (filledCells['TR'] == filledCells['CR'] && filledCells['CR'] == filledCells['BR'])) return true;
	if (filledCells['CL'] != undefined && (filledCells['CL'] == filledCells['CC'] && filledCells['CC'] == filledCells['CR'])) return true;
	if (filledCells['BL'] != undefined && (filledCells['BL'] == filledCells['BC'] && filledCells['BC'] == filledCells['BR'])) return true;
	return false;
};


// game interaction controllers

function cellClicked(args) {
	makeMove(cellIndex(args.data.id));
};

function makeMove(index) {
	// proceed only if the cell is free
	if (cells.getAt(index).content.value == '') {
		// mark the cell
		cells.getAt(index).content.value = player.value;
		// switch the player
		if (player.value == 'o') {
			player.value = 'x';
		} else {
			player.value = 'o';
		}
	}
};

function restartGame() {
	cells.forEach(function(cell) {
		cell.content.value = '';
	});
	player.value = 'o';
};


// game state handlers

var player = Observable('o');

var cellsOccupied = Observable(function() {
	var res = 0;
	cells.forEach(function(cell) {
		if (cell.content.value != '') res++;
	});
	return res;
});

var someoneWon = cellsOccupied.map(function(cnt) {
	if (cnt >= 5) return isInLine();
	return false;
});

var noMovesPossible = cellsOccupied.map(function(x) {
	return x == 9;
});

var gameCompleted = Observable(function() {
	return (noMovesPossible.value == true || someoneWon.value == true);
});


// module exports

module.exports = {
	'cellClicked': cellClicked,
	'cells': cells,
	'playerMove': player.map(function(x) { return 'Player move: ' + x; }),
	'gameCompleted': gameCompleted,
	'someoneWon': someoneWon,
	'noMovesPossible': noMovesPossible,
	'restartGame': restartGame
};
